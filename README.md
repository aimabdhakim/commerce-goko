# Commerce Goko

This code was created to fulfill technical test tasks from Gokomodo.

For all API, it was listed on file postman.json, you can import it straight from the file.

For the table, you can see all the table in table-list.txt

On how to use the code, you have to clone it first (of course man...), and then you have to change the file (.env.example) in commerceapp and userapp folder to .env, and fill it depends on your local database.

For running the code, there's 3 different module here that you have to run. Before you run the code, please make sure that each one have different port. Example: for apigateway use port 8080, for userapp use 3003, for commerceapp use 3004. After that go to each folder and type "go run main.go"