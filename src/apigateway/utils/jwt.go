package utils

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"go-micro.dev/v4/logger"
)

type SecretKey struct {
	PublicKey                   *rsa.PublicKey
	PrivateKey                  *rsa.PrivateKey
	ExpiredTokenInMinute        int
	ExpiredRefreshTokenInMinute int
}

type UserPayload struct {
	UserID     string `json:"user_id"`
	Username   string `json:"username"`
	Email      string `json:"email"`
	Name       string `json:"name"`
	Address    string `json:"address"`
	UserTypeID int    `json:"usertypeid"`
}

func NewSecretKey(ttlToken int, ttlRefresh int, bits ...int) *SecretKey {
	var bitsValue = 1024
	if len(bits) > 0 {
		bitsValue = bits[0]
	}

	privateKey, err := rsa.GenerateKey(rand.Reader, bitsValue)
	if err != nil {
		logger.Error("fail create jwt secret key, ", err)
		return nil
	}

	return &SecretKey{
		PublicKey:                   &privateKey.PublicKey,
		PrivateKey:                  privateKey,
		ExpiredTokenInMinute:        ttlToken,
		ExpiredRefreshTokenInMinute: ttlRefresh,
	}
}

func CreateToken(ttl time.Duration, payload *UserPayload, privateKey *rsa.PrivateKey) (string, error) {
	now := time.Now()

	claims := make(jwt.MapClaims)
	claims["data"] = payload
	claims["exp"] = now.Add(ttl).Unix()
	claims["iat"] = now.Unix()
	claims["nbf"] = now.Unix()

	token, err := jwt.NewWithClaims(jwt.SigningMethodRS256, claims).SignedString(privateKey)

	if err != nil {
		return "", fmt.Errorf("create: sign token: %w", err)
	}

	return token, nil
}

func ValidateToken(token string, publicKey *rsa.PublicKey) (jwt.MapClaims, error) {
	parsedToken, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected method: %s", t.Header["alg"])
		}
		return publicKey, nil
	})

	if err != nil {
		logger.Warn("fail verify jwt token: ", err)
		return nil, fmt.Errorf("%w", err)
	}

	claims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok || !parsedToken.Valid {
		return nil, fmt.Errorf("invalid token")
	}
	return claims, nil
}
