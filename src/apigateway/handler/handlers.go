package handler

import (
	"context"

	"github.com/gin-gonic/gin"

	pb "apigateway/proto"

	"apigateway/config"
	"apigateway/middleware"
	"apigateway/utils"
)

type APIGatewayHandler interface {
	Route(*gin.Engine)
}

type apiGatewayServer struct {
	svcUser     pb.UserAppService
	svcCommerce pb.CommerceAppService
}

func NewAPIGatewayServer(svcUser pb.UserAppService, svcCommerce pb.CommerceAppService) *apiGatewayServer {
	return &apiGatewayServer{
		svcUser:     svcUser,
		svcCommerce: svcCommerce,
	}
}

type apiGatewayImpl struct {
	cfg    *config.Config
	ctx    context.Context
	server *apiGatewayServer
}

func (_h *apiGatewayImpl) Route(r *gin.Engine) {
	var response = &utils.Response{}
	r.NoRoute(func(c *gin.Context) {
		response.SetNotFound(c)
	})
	r.NoMethod(func(c *gin.Context) {
		response.SetMethodNotAllowed(c)
	})

	// User Route
	userRoute := r.Group("user")
	userRoute.POST("/register", _h.Register)
	userRoute.POST("/login", _h.UserLogin)
	userRoute.GET("/profile", middleware.Protected(), _h.UserProfile)

	// Commerce App Route
	r.GET("/product", middleware.Protected(), _h.ListProduct)
	r.POST("/product", middleware.Protected(), _h.AddProduct)
	r.PUT("/product", middleware.Protected(), _h.UpdateProduct)
	carouselRoute := r.Group("/product")
	carouselRoute.GET("/:product_id", middleware.Protected(), _h.DetailProduct)
	carouselRoute.DELETE("/:product_id", middleware.Protected(), _h.DeleteProduct)

}

func NewAPIGatewayImpl(ctx context.Context, svc *apiGatewayServer, cfg *config.Config) APIGatewayHandler {
	return &apiGatewayImpl{
		ctx:    ctx,
		server: svc,
		cfg:    cfg,
	}
}
