package handler

import (
	"apigateway/utils"
	"time"

	pb "apigateway/proto"

	"github.com/gin-gonic/gin"
	"go-micro.dev/v4/logger"
)

func (_h *apiGatewayImpl) UserProfile(c *gin.Context) {
	userCtx, ok := c.Get("user")
	var response = &utils.Response{}
	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}
	userPayload, _ := userCtx.(*utils.UserPayload)
	resp, err := _h.server.svcUser.Profile(_h.ctx, &pb.UserDetailRequest{UserId: userPayload.UserID})
	if err != nil || resp == nil {
		logger.Error("fail login, ", err)
		response.SetServerUnavailable(c)
		return
	}
	user := &utils.User{}
	user.Load(resp.Data)
	response.Data = user
	response.SetOK(c)
}

func (_h *apiGatewayImpl) Register(c *gin.Context) {
	var err error
	var request = &pb.UserRegisterRequest{}
	var response = &utils.Response{}

	err = c.Bind(request)
	if err != nil {
		logger.Error("error to get request data, ", err)
		response.SetBadRequestError(c, "request not valid")
		return
	}

	resp, err := _h.server.svcUser.Register(_h.ctx, request)
	if err != nil {
		logger.Error("authentication failed, ", err)
		response.SetServerUnavailable(c)
		return
	}

	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}

	response.Message = resp.Msg
	response.SetOK(c)
}

func (_h *apiGatewayImpl) UserLogin(c *gin.Context) {
	var err error
	var request = &pb.UserLoginRequest{}
	var response = &utils.Response{}

	err = c.Bind(request)
	if err != nil {
		logger.Error("failed to get request data, ", err)
		response.SetBadRequestError(c, "request not valid")
		return
	}
	resp, err := _h.server.svcUser.Login(_h.ctx, request)
	if err != nil || resp == nil {
		logger.Error("failed to login, ", err)
		response.SetServerUnavailable(c)
		return
	}
	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}

	expToken := time.Duration(_h.cfg.SecretKey.ExpiredTokenInMinute) * time.Minute
	user := resp.Data
	payload := &utils.UserPayload{
		UserID:     user.UserId,
		Username:   user.Username,
		Email:      user.Email,
		Name:       user.Name,
		Address:    user.Address,
		UserTypeID: int(user.Usertypeid),
	}

	token, err := utils.CreateToken(expToken, payload, _h.cfg.SecretKey.PrivateKey)
	if err != nil {
		logger.Error("fail create access token, ", err)
		response.SetServerError(c)
		return
	}
	expTokenUnix := time.Now().Add(time.Duration(_h.cfg.SecretKey.ExpiredTokenInMinute) * time.Minute).Unix()
	response.Data = gin.H{
		"access_token":     token,
		"exp_access_token": expTokenUnix,
	}
	response.Message = resp.Msg
	response.SetOK(c)
}
