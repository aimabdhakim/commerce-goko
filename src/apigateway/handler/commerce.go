package handler

import (
	pb "apigateway/proto"
	util "apigateway/utils"
	"utils"

	"github.com/gin-gonic/gin"
	"go-micro.dev/v4/logger"
)

const (
	UserTypeIDMember int = 1
	UserTypeIDSales  int = 2
)

func (_h *apiGatewayImpl) ListProduct(c *gin.Context) {
	_, ok := c.Get("user")
	var response = &util.Response{}
	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}

	type Param struct {
		Page    float32 `form:"page" json:"page" query:"page"`
		Keyword string  `form:"keyword" json:"keyword" query:"keyword"`
		Limit   int     `form:"limit" json:"limit" query:"limit"`
	}
	var param = &Param{}
	var request = &pb.GetProductRequest{}

	if err := c.ShouldBindQuery(&param); err != nil {
		response.SetNotAcceptable(c, err.Error())
		return
	}

	pagination, _ := utils.TypeConverter[pb.PaginationRequest](param)
	if param != nil {
		request.Pagination = pagination
	}
	resp, err := _h.server.svcCommerce.ListProduct(_h.ctx, request)
	if err != nil {
		logger.Error("failed to get product data, ", err)
		response.SetServerUnavailable(c)
		return
	}
	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}
	response.Message = resp.Msg
	response.Data = resp.Data
	response.SetOK(c)
}

func (_h *apiGatewayImpl) DetailProduct(c *gin.Context) {
	userCtx, ok := c.Get("user")
	var response = &util.Response{}

	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}
	_, ok = userCtx.(*util.UserPayload)
	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}

	type Param struct {
		ProductID string `uri:"product_id" binding:"required"`
	}
	var param = &Param{}
	if err := c.ShouldBindUri(&param); err != nil {
		logger.Error("error bind uri: ", err)
		response.SetBadRequestError(c, "you must specify product id")
		return
	}

	var request = &pb.GetProductRequest{
		ProductId: param.ProductID,
	}

	resp, err := _h.server.svcCommerce.DetailProduct(_h.ctx, request)
	if err != nil {
		logger.Error(err)
		response.SetServerUnavailable(c)
		return
	}
	if !resp.Status {
		logger.Warn("fail to get detail product : ", resp.Msg)
		response.SetNotFound(c)
		return
	}

	response.Data = resp.Data
	response.SetOK(c)
}

func (_h *apiGatewayImpl) AddProduct(c *gin.Context) {
	userCtx, ok := c.Get("user")
	var response = &util.Response{}
	var err error

	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}
	userPayload, _ := userCtx.(*util.UserPayload)
	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}

	if userPayload.UserTypeID != UserTypeIDSales {
		response.SetUnauthorized(c, "not a sales")
		return
	}

	var request = &pb.ProductRequest{}

	err = c.Bind(request)
	if err != nil {
		logger.Error("error to get request data, ", err)
		response.SetBadRequestError(c, "request not valid")
		return
	}
	request.OwnerId = userPayload.UserID
	request.OwnerName = userPayload.Name

	resp, err := _h.server.svcCommerce.AddProduct(_h.ctx, request)
	if err != nil {
		logger.Error("failed to add data, ", err)
		response.SetServerUnavailable(c)
		return
	}

	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}

	response.Message = resp.Msg
	response.Data = resp.Data
	response.SetOK(c)
}

func (_h *apiGatewayImpl) UpdateProduct(c *gin.Context) {
	userCtx, ok := c.Get("user")
	var response = &util.Response{}
	var err error

	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}
	userPayload, _ := userCtx.(*util.UserPayload)
	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}

	if userPayload.UserTypeID != UserTypeIDSales {
		response.SetUnauthorized(c, "not a sales")
		return
	}

	var request = &pb.ProductRequest{}

	err = c.Bind(request)
	if err != nil {
		logger.Error("error to get request data, ", err)
		response.SetBadRequestError(c, "request not valid")
		return
	}
	request.OwnerId = userPayload.UserID
	request.OwnerName = userPayload.Name

	resp, err := _h.server.svcCommerce.UpdateProduct(_h.ctx, request)
	if err != nil {
		logger.Error("failed to update data, ", err)
		response.SetServerUnavailable(c)
		return
	}

	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}

	response.Message = resp.Msg
	response.Data = resp.Data
	response.SetOK(c)
}

func (_h *apiGatewayImpl) DeleteProduct(c *gin.Context) {
	userCtx, ok := c.Get("user")
	var response = &util.Response{}
	var err error

	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}
	userPayload, _ := userCtx.(*util.UserPayload)
	if !ok {
		response.SetUnauthorized(c, "token expired")
		return
	}

	if userPayload.UserTypeID != UserTypeIDSales {
		response.SetUnauthorized(c, "not a sales")
		return
	}

	type Param struct {
		ProductID string `uri:"product_id" binding:"required"`
	}
	var param = &Param{}
	if err := c.ShouldBindUri(&param); err != nil {
		logger.Error("error bind uri: ", err)
		response.SetBadRequestError(c, "you must specify product id")
		return
	}

	var request = &pb.DetailRequest{
		ProductId: param.ProductID,
		OwnerId:   userPayload.UserID,
	}

	resp, err := _h.server.svcCommerce.DeleteProduct(_h.ctx, request)
	if err != nil {
		logger.Error("failed to delete data, ", err)
		response.SetServerUnavailable(c)
		return
	}

	if !resp.Status {
		response.SetBadRequestError(c, resp.GetMsg())
		return
	}

	response.Message = resp.Msg
	response.SetOK(c)
}
