package config

import (
	"apigateway/utils"
	"fmt"
	"os"
	"strconv"

	"github.com/pkg/errors"
	"go-micro.dev/v4/config"
	"go-micro.dev/v4/config/source/env"
	"go-micro.dev/v4/logger"
)

type Config struct {
	Port            int
	UserService     string
	CommerceService string
	SecretKey       *utils.SecretKey
}

var cfg *Config = &Config{
	Port:            GetEnvInt("PORT", 8080),
	UserService:     "userapp",
	CommerceService: "commerceapp",
	SecretKey:       utils.NewSecretKey(GetEnvInt("ACCESS_TOKEN_EXPIRE", 1440), GetEnvInt("REFRESH_TOKEN_EXPIRE", 7200)),
}

func Get() *Config {
	return cfg
}

func GetEnv(key string, defaultval ...string) string {
	val := os.Getenv(key)
	if val != "" {
		return val
	}
	if len(defaultval) > 0 {
		return defaultval[0]
	}
	return ""
}

func GetEnvInt(key string, defaultval ...int) int {
	val := os.Getenv(key)
	if val != "" {
		valInt, err := strconv.Atoi(val)
		if err != nil {
			logger.Fatal("fail to load or convert env var", val, ":", err)
		}
		return valInt
	}
	if len(defaultval) > 0 {
		return defaultval[0]
	}
	return 0
}

func Address() string {
	return fmt.Sprintf(":%d", cfg.Port)
}

func Load() error {
	configor, err := config.NewConfig(config.WithSource(env.NewSource()))
	if err != nil {
		return errors.Wrap(err, "configor.New")
	}
	if err := configor.Load(); err != nil {
		return errors.Wrap(err, "configor.Load")
	}
	if err := configor.Scan(cfg); err != nil {
		return errors.Wrap(err, "configor.Scan")
	}
	return nil
}
