// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: proto/apigateway.proto

package apigateway

import (
	fmt "fmt"
	proto "google.golang.org/protobuf/proto"
	math "math"
)

import (
	context "context"
	api "go-micro.dev/v4/api"
	client "go-micro.dev/v4/client"
	server "go-micro.dev/v4/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// Reference imports to suppress errors if they are not otherwise used.
var _ api.Endpoint
var _ context.Context
var _ client.Option
var _ server.Option

// Api Endpoints for UserApp service

func NewUserAppEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for UserApp service

type UserAppService interface {
	Login(ctx context.Context, in *UserLoginRequest, opts ...client.CallOption) (*UserResponse, error)
	Register(ctx context.Context, in *UserRegisterRequest, opts ...client.CallOption) (*UserRegisterResponse, error)
	Profile(ctx context.Context, in *UserDetailRequest, opts ...client.CallOption) (*UserResponse, error)
}

type userAppService struct {
	c    client.Client
	name string
}

func NewUserAppService(name string, c client.Client) UserAppService {
	return &userAppService{
		c:    c,
		name: name,
	}
}

func (c *userAppService) Login(ctx context.Context, in *UserLoginRequest, opts ...client.CallOption) (*UserResponse, error) {
	req := c.c.NewRequest(c.name, "UserApp.Login", in)
	out := new(UserResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userAppService) Register(ctx context.Context, in *UserRegisterRequest, opts ...client.CallOption) (*UserRegisterResponse, error) {
	req := c.c.NewRequest(c.name, "UserApp.Register", in)
	out := new(UserRegisterResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userAppService) Profile(ctx context.Context, in *UserDetailRequest, opts ...client.CallOption) (*UserResponse, error) {
	req := c.c.NewRequest(c.name, "UserApp.Profile", in)
	out := new(UserResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for UserApp service

type UserAppHandler interface {
	Login(context.Context, *UserLoginRequest, *UserResponse) error
	Register(context.Context, *UserRegisterRequest, *UserRegisterResponse) error
	Profile(context.Context, *UserDetailRequest, *UserResponse) error
}

func RegisterUserAppHandler(s server.Server, hdlr UserAppHandler, opts ...server.HandlerOption) error {
	type userApp interface {
		Login(ctx context.Context, in *UserLoginRequest, out *UserResponse) error
		Register(ctx context.Context, in *UserRegisterRequest, out *UserRegisterResponse) error
		Profile(ctx context.Context, in *UserDetailRequest, out *UserResponse) error
	}
	type UserApp struct {
		userApp
	}
	h := &userAppHandler{hdlr}
	return s.Handle(s.NewHandler(&UserApp{h}, opts...))
}

type userAppHandler struct {
	UserAppHandler
}

func (h *userAppHandler) Login(ctx context.Context, in *UserLoginRequest, out *UserResponse) error {
	return h.UserAppHandler.Login(ctx, in, out)
}

func (h *userAppHandler) Register(ctx context.Context, in *UserRegisterRequest, out *UserRegisterResponse) error {
	return h.UserAppHandler.Register(ctx, in, out)
}

func (h *userAppHandler) Profile(ctx context.Context, in *UserDetailRequest, out *UserResponse) error {
	return h.UserAppHandler.Profile(ctx, in, out)
}

// Api Endpoints for CommerceApp service

func NewCommerceAppEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for CommerceApp service

type CommerceAppService interface {
	ListProduct(ctx context.Context, in *GetProductRequest, opts ...client.CallOption) (*ListProductResponse, error)
	DetailProduct(ctx context.Context, in *GetProductRequest, opts ...client.CallOption) (*DetailProductResponse, error)
	AddProduct(ctx context.Context, in *ProductRequest, opts ...client.CallOption) (*DetailProductResponse, error)
	UpdateProduct(ctx context.Context, in *ProductRequest, opts ...client.CallOption) (*DetailProductResponse, error)
	DeleteProduct(ctx context.Context, in *DetailRequest, opts ...client.CallOption) (*Response, error)
}

type commerceAppService struct {
	c    client.Client
	name string
}

func NewCommerceAppService(name string, c client.Client) CommerceAppService {
	return &commerceAppService{
		c:    c,
		name: name,
	}
}

func (c *commerceAppService) ListProduct(ctx context.Context, in *GetProductRequest, opts ...client.CallOption) (*ListProductResponse, error) {
	req := c.c.NewRequest(c.name, "CommerceApp.ListProduct", in)
	out := new(ListProductResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *commerceAppService) DetailProduct(ctx context.Context, in *GetProductRequest, opts ...client.CallOption) (*DetailProductResponse, error) {
	req := c.c.NewRequest(c.name, "CommerceApp.DetailProduct", in)
	out := new(DetailProductResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *commerceAppService) AddProduct(ctx context.Context, in *ProductRequest, opts ...client.CallOption) (*DetailProductResponse, error) {
	req := c.c.NewRequest(c.name, "CommerceApp.AddProduct", in)
	out := new(DetailProductResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *commerceAppService) UpdateProduct(ctx context.Context, in *ProductRequest, opts ...client.CallOption) (*DetailProductResponse, error) {
	req := c.c.NewRequest(c.name, "CommerceApp.UpdateProduct", in)
	out := new(DetailProductResponse)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *commerceAppService) DeleteProduct(ctx context.Context, in *DetailRequest, opts ...client.CallOption) (*Response, error) {
	req := c.c.NewRequest(c.name, "CommerceApp.DeleteProduct", in)
	out := new(Response)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for CommerceApp service

type CommerceAppHandler interface {
	ListProduct(context.Context, *GetProductRequest, *ListProductResponse) error
	DetailProduct(context.Context, *GetProductRequest, *DetailProductResponse) error
	AddProduct(context.Context, *ProductRequest, *DetailProductResponse) error
	UpdateProduct(context.Context, *ProductRequest, *DetailProductResponse) error
	DeleteProduct(context.Context, *DetailRequest, *Response) error
}

func RegisterCommerceAppHandler(s server.Server, hdlr CommerceAppHandler, opts ...server.HandlerOption) error {
	type commerceApp interface {
		ListProduct(ctx context.Context, in *GetProductRequest, out *ListProductResponse) error
		DetailProduct(ctx context.Context, in *GetProductRequest, out *DetailProductResponse) error
		AddProduct(ctx context.Context, in *ProductRequest, out *DetailProductResponse) error
		UpdateProduct(ctx context.Context, in *ProductRequest, out *DetailProductResponse) error
		DeleteProduct(ctx context.Context, in *DetailRequest, out *Response) error
	}
	type CommerceApp struct {
		commerceApp
	}
	h := &commerceAppHandler{hdlr}
	return s.Handle(s.NewHandler(&CommerceApp{h}, opts...))
}

type commerceAppHandler struct {
	CommerceAppHandler
}

func (h *commerceAppHandler) ListProduct(ctx context.Context, in *GetProductRequest, out *ListProductResponse) error {
	return h.CommerceAppHandler.ListProduct(ctx, in, out)
}

func (h *commerceAppHandler) DetailProduct(ctx context.Context, in *GetProductRequest, out *DetailProductResponse) error {
	return h.CommerceAppHandler.DetailProduct(ctx, in, out)
}

func (h *commerceAppHandler) AddProduct(ctx context.Context, in *ProductRequest, out *DetailProductResponse) error {
	return h.CommerceAppHandler.AddProduct(ctx, in, out)
}

func (h *commerceAppHandler) UpdateProduct(ctx context.Context, in *ProductRequest, out *DetailProductResponse) error {
	return h.CommerceAppHandler.UpdateProduct(ctx, in, out)
}

func (h *commerceAppHandler) DeleteProduct(ctx context.Context, in *DetailRequest, out *Response) error {
	return h.CommerceAppHandler.DeleteProduct(ctx, in, out)
}
