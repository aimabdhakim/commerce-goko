package main

import (
	"apigateway/config"
	"apigateway/handler"
	"apigateway/middleware"
	pb "apigateway/proto"

	"context"
	"os"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-micro/cli/debug/trace/jaeger"
	ot "github.com/go-micro/plugins/v4/wrapper/trace/opentracing"
	"github.com/sirupsen/logrus"
	"go-micro.dev/v4"
	"go-micro.dev/v4/logger"
	"go-micro.dev/v4/server"

	grpcc "github.com/go-micro/plugins/v4/client/grpc"
	https "github.com/go-micro/plugins/v4/server/http"
)

var (
	name    = "apigateway"
	version = "1.0"
)

func main() {
	// Load configuration
	if err := config.Load(); err != nil {
		logger.Fatal(err)
	}

	// Create tracer
	tracer, closer, err := jaeger.NewTracer(
		jaeger.Name(name),
		jaeger.FromEnv(true),
		jaeger.GlobalTracer(true),
	)
	if err != nil {
		logger.Fatal(err)
	}
	defer closer.Close()

	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())

	// Creating services
	srv := micro.NewService(
		micro.Server(https.NewServer()),
		micro.Client(grpcc.NewClient(grpcc.MaxSendMsgSize(1024*1024*20))),
		micro.BeforeStart(func() error {
			logger.Infof("Starting service %s", name)
			return nil
		}),
		micro.BeforeStop(func() error {
			logger.Infof("Shutting down service %s", name)
			cancel()
			return nil
		}),
		micro.AfterStop(func() error {
			wg.Wait()
			return nil
		}),
		micro.WrapCall(ot.NewCallWrapper(tracer)),
		micro.WrapClient(ot.NewClientWrapper(tracer)),
		micro.WrapHandler(ot.NewHandlerWrapper(tracer)),
		micro.WrapSubscriber(ot.NewSubscriberWrapper(tracer)),
	)
	opts := []micro.Option{
		micro.Name(name),
		micro.Version(version),
		micro.Address(config.Address()),
	}
	srv.Init(opts...)
	srv.Server().Init(
		server.Wait(&wg),
	)

	ctx = server.NewContext(ctx, srv.Server())

	log := logrus.New()
	log.Level = logrus.DebugLevel
	log.Formatter = &logrus.JSONFormatter{
		FieldMap: logrus.FieldMap{
			logrus.FieldKeyTime:  "timestamp",
			logrus.FieldKeyLevel: "severity",
			logrus.FieldKeyMsg:   "message",
		},
		TimestampFormat: time.RFC3339Nano,
	}
	log.Out = os.Stdout

	cfg, client := config.Get(), srv.Client()
	svc := handler.NewAPIGatewayServer(
		pb.NewUserAppService(cfg.UserService, client),
		pb.NewCommerceAppService(cfg.CommerceService, client),
	)

	r := gin.Default()
	r.Use(
		middleware.CORSMiddleware(),
		middleware.ErrorHandler,
		// middleware.XssRemove,
		gin.Recovery(),
	)
	r.HandleMethodNotAllowed = true
	handler.NewAPIGatewayImpl(ctx, svc, cfg).Route(r)

	if err := micro.RegisterHandler(srv.Server(), r); err != nil {
		logger.Fatal(err)
	}
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
