package middleware

import (
	"apigateway/config"
	"apigateway/utils"
	"crypto/rsa"
	"encoding/json"
	"errors"
	"strings"

	"github.com/dvwright/xss-mw"
	"github.com/gin-gonic/gin"
	"go-micro.dev/v4/logger"
)

func ErrorHandler(c *gin.Context) {
	c.Next()
	for _, err := range c.Errors {
		logger.Error(err)
	}
}

func XssRemove(c *gin.Context) {
	var xssMdlwr xss.XssMw
	err := xssMdlwr.XssRemove(c)
	if err != nil {
		logger.Error(err)
		c.AbortWithStatusJSON(400, gin.H{"error": "invalid request"})
		return
	}
	c.Next()
}

func Protected() gin.HandlerFunc {
	return func(c *gin.Context) {
		var accessToken string
		var response utils.Response
		cookie, err := c.Cookie("access_token")

		authorizationHeader := c.Request.Header.Get("Authorization")
		fields := strings.Fields(authorizationHeader)
		if len(fields) != 0 && fields[0] == "Bearer" {
			accessToken = fields[1]
		} else if err == nil {
			accessToken = cookie
		}
		if accessToken == "" {
			response.SetUnauthorized(c, "you are not logged in")
			return
		}
		secretKey := config.Get().SecretKey
		claims, err := utils.ValidateToken(accessToken, secretKey.PublicKey)
		if err != nil {
			if errors.Is(err, rsa.ErrVerification) {
				response.SetUnauthorized(c, "invalid token")
				return
			}
			response.SetUnauthorized(c, err.Error())
			return
		}

		var userPayload = &utils.UserPayload{}
		bClaim, err := json.Marshal(claims["data"])
		if err != nil {
			logger.Error(err)
			response.SetUnauthorized(c, "invalid token")
			return
		}
		if err := json.Unmarshal(bClaim, userPayload); err != nil {
			logger.Error(err)
			response.SetUnauthorized(c, "invalid token")
			return
		}
		c.Set("user", userPayload)
		c.Next()
	}
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// c.Writer.Header().Set("Content-Encoding", "br")
		// c.Writer.Header().Set("Transfer-Encoding", "chunked")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
