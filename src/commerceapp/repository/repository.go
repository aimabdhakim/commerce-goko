package repository

import (
	"commerceapp/entity"
	"context"
	"utils"

	"gorm.io/gorm"
)

type CommerceRepository interface {
	ListProduct(ctx context.Context, pagination *utils.Pagination) ([]*entity.Product, error)
	GetProductByID(ctx context.Context, productID string) (*entity.Product, error)
	AddProduct(ctx context.Context, product *entity.Product) error
	UpdateProduct(ctx context.Context, product *entity.Product) error
	DeleteProduct(ctx context.Context, productID string) error
	GetDB() *gorm.DB
}

type commerceRepositoryImpl struct {
	db *gorm.DB
}

func (_r *commerceRepositoryImpl) ListProduct(ctx context.Context, pagination *utils.Pagination) (products []*entity.Product, err error) {
	// get table name
	var product = &entity.Product{}

	// default pagination sort is ascending and the column is created_at
	pagination.Sort = "asc"
	result := _r.db.Table(product.TableName()).WithContext(ctx).Scopes(utils.Paginate(products, pagination, _r.db, "created_at"))

	err = result.Find(&products).Error
	if len(products) == 0 {
		err = gorm.ErrRecordNotFound
		return
	}
	return
}

func (_r *commerceRepositoryImpl) GetProductByID(ctx context.Context, productID string) (*entity.Product, error) {
	product := &entity.Product{}
	result := _r.db.WithContext(ctx).Table(product.TableName()).Where("product_id = ?", productID).First(&product)
	if result.Error != nil {
		return nil, result.Error
	}
	return product, nil
}

func (_r *commerceRepositoryImpl) AddProduct(ctx context.Context, product *entity.Product) error {
	err := _r.db.WithContext(ctx).Table(product.TableName()).Create(&product).Error
	if err != nil {
		return err
	}
	return nil
}

func (_r *commerceRepositoryImpl) UpdateProduct(ctx context.Context, product *entity.Product) error {
	err := _r.db.WithContext(ctx).Where("product_id = ?", product.ProductID).UpdateColumns(&product).Error
	if err != nil {
		return err
	}
	return nil
}

func (_r *commerceRepositoryImpl) DeleteProduct(ctx context.Context, productID string) error {
	var product = &entity.Product{}

	if err := _r.db.WithContext(ctx).Where("product_id = ?", productID).Delete(product).Error; err != nil {
		return err
	}
	return nil
}

func NewCommerceRepositoryImpl(db *gorm.DB) CommerceRepository {
	return &commerceRepositoryImpl{
		db: db,
	}
}

func (_r *commerceRepositoryImpl) GetDB() *gorm.DB {
	return _r.db
}
