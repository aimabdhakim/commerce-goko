package entity

import (
	pb "commerceapp/proto"
	"time"

	"gorm.io/gorm"
)

const productTableName string = "product"

type Product struct {
	ProductID   string         `gorm:"column:product_id;default:uuid_generate_v4()"`
	Name        string         `gorm:"column:name"`
	Description string         `gorm:"column:description"`
	Price       int            `gorm:"column:price"`
	OwnerID     string         `gorm:"column:owner_id"`
	OwnerName   string         `gorm:"column:owner_name"`
	CreatedBy   string         `gorm:"column:created_by"`
	ModifiedBy  string         `gorm:"column:modified_by"`
	DeletedBy   string         `gorm:"column:deleted_by"`
	CreatedAt   time.Time      `gorm:"column:created_at"`
	ModifiedAt  time.Time      `gorm:"column:modified_at"`
	DeletedAt   gorm.DeletedAt `gorm:"column:deleted_at"`
}

func (Product) TableName() string {
	return productTableName
}

func (_p *Product) ToPB(in *pb.Product) {
	in.ProductId = _p.ProductID
	in.Name = _p.Name
	in.Description = _p.Description
	in.Price = int32(_p.Price)
	in.CreatedBy = _p.CreatedBy
	in.ModifiedBy = _p.ModifiedBy
	in.CreatedAt = _p.CreatedAt.String()
	in.ModifiedAt = _p.ModifiedAt.String()
}
