package service

import (
	"context"
	"errors"
	"time"

	"commerceapp/entity"
	pb "commerceapp/proto"
	"commerceapp/repository"
	"commerceapp/validation"
	"utils"

	"go-micro.dev/v4/logger"
	"gorm.io/gorm"
)

var (
	ErrRequiredProductID      = errors.New("product id is required")
	ErrProductNotFound        = errors.New("product not found")
	ErrFailToSaveProduct      = errors.New("fail to save product")
	ErrFailToUpdateProduct    = errors.New("fail to update product")
	ErrFailToDeleteProduct    = errors.New("fail to delete product")
	ErrProductIDCannotBeEmpty = errors.New("product id cannot be empty")
)

type CommerceService interface {
	ListProduct(ctx context.Context, in *pb.GetProductRequest, resp *pb.ListProductResponse) error
	DetailProduct(ctx context.Context, in *pb.GetProductRequest, resp *pb.DetailProductResponse) error
	AddProduct(ctx context.Context, in *pb.ProductRequest, resp *pb.DetailProductResponse) error
	UpdateProduct(ctx context.Context, in *pb.ProductRequest, resp *pb.DetailProductResponse) error
	DeleteProduct(ctx context.Context, in *pb.DetailRequest, resp *pb.Response) error
}

type commerceServiceImpl struct {
	repository repository.CommerceRepository
}

func (_s *commerceServiceImpl) ListProduct(ctx context.Context, in *pb.GetProductRequest, resp *pb.ListProductResponse) error {
	pagination := utils.NewPagination()
	pagination.Load(in.Pagination)
	if err := pagination.Validate(); err != nil {
		logger.Error(err)
		return err
	}

	products, err := _s.repository.ListProduct(ctx, pagination)
	if err != nil {
		logger.Error(err)
		return err
	}

	var productResp []*pb.Product
	r := &utils.PaginationResponse{}
	r.Default(pagination)

	paginationResp, _ := utils.TypeConverter[pb.PaginationResponse](r)

	for _, v := range products {
		var product = pb.Product{}
		v.ToPB(&product)
		productResp = append(productResp, &product)
	}
	dataResp := &pb.ProductPaginationResponse{
		Result:     productResp,
		Pagination: paginationResp,
	}
	resp.Data = dataResp

	return nil
}

func (_s *commerceServiceImpl) DetailProduct(ctx context.Context, in *pb.GetProductRequest, resp *pb.DetailProductResponse) error {
	if in.GetProductId() == "" {
		return ErrRequiredProductID
	}

	product, err := _s.repository.GetProductByID(ctx, in.ProductId)
	if err != nil {
		logger.Error("product not found")
		return ErrProductNotFound
	}

	productData := &pb.Product{}
	product.ToPB(productData)

	resp.Data = productData

	return err
}

func (_s *commerceServiceImpl) AddProduct(ctx context.Context, in *pb.ProductRequest, resp *pb.DetailProductResponse) error {
	var err error
	var request = &validation.ProductRequest{}
	var product = &entity.Product{}
	request.Load(in)

	if err := request.Validate(ctx); err != nil {
		return err
	}

	request.ToEntity(product)
	product.CreatedAt = time.Now()
	product.CreatedBy = request.OwnerID

	err = _s.repository.AddProduct(ctx, product)
	if err != nil {
		logger.Error("failed to save product, ", err)
		err = nil
		return ErrFailToSaveProduct
	}

	productData := &pb.Product{}
	product.ToPB(productData)
	resp.Data = productData

	return err
}

func (_s *commerceServiceImpl) UpdateProduct(ctx context.Context, in *pb.ProductRequest, resp *pb.DetailProductResponse) error {
	var err error
	var request = &validation.ProductRequest{}
	var product = &entity.Product{}
	request.Load(in)
	if err := request.Validate(ctx); err != nil {
		return err
	}

	if request.ProductID == "" {
		logger.Error("product id cannot empty")
		return ErrProductIDCannotBeEmpty
	}

	tempProduct, err := _s.repository.GetProductByID(ctx, request.ProductID)
	if tempProduct == nil || err == gorm.ErrRecordNotFound {
		logger.Error("product not found, ", err)
		return ErrProductNotFound
	}

	request.ToEntity(product)
	product.ModifiedAt = time.Now()
	product.ModifiedBy = request.OwnerID
	product.ProductID = request.ProductID

	err = _s.repository.UpdateProduct(ctx, product)
	if err != nil {
		logger.Error("failed to update product, ", err)
		err = nil
		return ErrFailToUpdateProduct
	}

	productData := &pb.Product{}
	product.ToPB(productData)
	resp.Data = productData

	return err
}

func (_s *commerceServiceImpl) DeleteProduct(ctx context.Context, in *pb.DetailRequest, resp *pb.Response) error {
	var err error
	var request = &validation.DetailRequest{}
	request.Load(in)
	if err = request.Validate(ctx); err != nil {
		return err
	}

	if request.ProductID == "" {
		logger.Error("product id cannot empty")
		return ErrProductIDCannotBeEmpty
	}

	tempProduct, err := _s.repository.GetProductByID(ctx, request.ProductID)
	if tempProduct == nil || err == gorm.ErrRecordNotFound {
		logger.Error("product not found, ", err)
		return ErrProductNotFound
	}

	err = _s.repository.DeleteProduct(ctx, request.ProductID)
	if err != nil {
		logger.Error("failed to delete product, ", err)
		err = nil
		return ErrFailToDeleteProduct
	}

	return nil
}

func NewCommerceServiceImpl(repository repository.CommerceRepository) CommerceService {
	return &commerceServiceImpl{
		repository: repository,
	}
}
