package validation

import (
	"commerceapp/entity"
	pb "commerceapp/proto"
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type ProductRequest struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Price       int    `json:"price"`
	OwnerID     string `json:"owner_id"`
	OwnerName   string `json:"owner_name"`
	ProductID   string `json:"product_id"`
}

type DetailRequest struct {
	OwnerID   string `json:"owner_id"`
	ProductID string `json:"product_id"`
}

func (_p *ProductRequest) Load(in *pb.ProductRequest) {
	_p.Name = in.Name
	_p.Description = in.Description
	_p.Price = int(in.Price)
	_p.OwnerID = in.OwnerId
	_p.OwnerName = in.OwnerName
	_p.ProductID = in.ProductId
}

func (_p *ProductRequest) ToEntity(product *entity.Product) {
	product.Name = _p.Name
	product.Description = _p.Description
	product.Price = _p.Price
	product.OwnerID = _p.OwnerID
	product.OwnerName = _p.OwnerName
	product.ProductID = _p.ProductID
}

func (request ProductRequest) Validate(ctx context.Context) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Name, validation.Required),
		validation.Field(&request.Description, validation.Required),
		validation.Field(&request.Price),
		validation.Field(&request.OwnerID, validation.Required),
		validation.Field(&request.OwnerName, validation.Required),
	)
	return err
}

func (_p *DetailRequest) Load(in *pb.DetailRequest) {
	_p.OwnerID = in.OwnerId
	_p.ProductID = in.ProductId
}

func (request DetailRequest) Validate(ctx context.Context) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.OwnerID, validation.Required),
		validation.Field(&request.ProductID, validation.Required),
	)

	return err
}
