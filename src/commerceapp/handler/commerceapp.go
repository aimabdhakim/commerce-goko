package handler

import (
	"commerceapp/service"
	"context"

	pb "commerceapp/proto"
)

const (
	msgFailedToGetData string = "failed to get list of product data"
	msgOk              string = "ok"
)

type CommerceApp struct {
	service service.CommerceService
}

func (_h *CommerceApp) ListProduct(ctx context.Context, in *pb.GetProductRequest, resp *pb.ListProductResponse) error {
	resp.Status = false
	if err := _h.service.ListProduct(ctx, in, resp); err != nil {
		resp.Msg = err.Error()
	}
	if resp.Data == nil {
		resp.Msg = msgFailedToGetData
	}
	resp.Status = !resp.Status
	resp.Msg = msgOk
	return nil
}

func (_h *CommerceApp) DetailProduct(ctx context.Context, in *pb.GetProductRequest, resp *pb.DetailProductResponse) error {
	resp.Status = false
	if err := _h.service.DetailProduct(ctx, in, resp); err != nil {
		resp.Msg = err.Error()
	}
	if resp.Data == nil {
		resp.Msg = msgFailedToGetData
	}
	resp.Status = !resp.Status
	resp.Msg = msgOk
	return nil
}

func (_h *CommerceApp) AddProduct(ctx context.Context, in *pb.ProductRequest, resp *pb.DetailProductResponse) error {
	resp.Status = false
	err := _h.service.AddProduct(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = "ok"
	}
	return nil
}

func (_h *CommerceApp) UpdateProduct(ctx context.Context, in *pb.ProductRequest, resp *pb.DetailProductResponse) error {
	resp.Status = false
	err := _h.service.UpdateProduct(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = "ok"
	}
	return nil
}

func (_h *CommerceApp) DeleteProduct(ctx context.Context, in *pb.DetailRequest, resp *pb.Response) error {
	resp.Status = false
	err := _h.service.DeleteProduct(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = "ok"
	}
	return nil
}

func NewCommerceAppHandler(service service.CommerceService) pb.CommerceAppHandler {
	return &CommerceApp{
		service: service,
	}
}
