package entity

import (
	"database/sql"
	"time"
)

const (
	userTableName     string = "users"
	userTypeTableName string = "usertype"
)

type User struct {
	UserID     string       `gorm:"column:user_id;default:uuid_generate_v4()"`
	Username   string       `gorm:"column:username"`
	Name       string       `gorm:"column:name"`
	Email      string       `gorm:"column:email"`
	Password   string       `gorm:"column:password"`
	Address    string       `gorm:"column:address"`
	UserTypeID int          `gorm:"column:user_type_id"`
	CreatedAt  time.Time    `gorm:"column:created_at"`
	ModifiedAt time.Time    `gorm:"column:modified_at"`
	DeletedAt  sql.NullTime `gorm:"column:deleted_at"`
	CreatedBy  string       `gorm:"column:created_by"`
	ModifiedBy string       `gorm:"column:modified_by"`
	DeletedBy  string       `gorm:"column:deleted_by"`
	UserType   UserType     `gorm:"foreignKey:UserTypeID"`
}

type UserType struct {
	ID   int    `gorm:"column:id"`
	Name string `gorm:"column:name"`
}

func (User) TableName() string {
	return userTableName
}
