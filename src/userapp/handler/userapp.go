package handler

import (
	"context"
	pb "userapp/proto"
	"userapp/service"

	"go-micro.dev/v4/logger"
)

type UserApp struct {
	service service.UserService
}

func (_u *UserApp) Register(ctx context.Context, in *pb.UserRegisterRequest, resp *pb.UserRegisterResponse) error {
	resp.Status = false
	err := _u.service.Register(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
	} else {
		resp.Status = !resp.Status
		resp.Msg = "ok"
	}
	return nil
}

func (_u *UserApp) Login(ctx context.Context, in *pb.UserLoginRequest, resp *pb.UserResponse) error {
	resp.Status = false
	err := _u.service.Login(ctx, in, resp)
	if err != nil {
		resp.Msg = err.Error()
		return nil
	}
	resp.Status = !resp.Status
	resp.Msg = "ok"
	return nil
}

func (_u *UserApp) Profile(ctx context.Context, in *pb.UserDetailRequest, resp *pb.UserResponse) error {
	resp.Status = false
	err := _u.service.Profile(ctx, in, resp)
	if err != nil {
		logger.Error("fail to get profile with user id ", in.UserId, ", ", err)
		resp.Msg = err.Error()
		return nil
	}
	if resp.Data == nil {
		resp.Msg = "failed to get user data"
		return nil
	}
	resp.Status = !resp.Status
	resp.Msg = "ok"
	return nil
}

func NewUserAppHandler(userService service.UserService) pb.UserAppHandler {
	return &UserApp{
		service: userService,
	}
}
