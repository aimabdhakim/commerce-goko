package validation

import (
	"context"
	"errors"
	"userapp/entity"
	pb "userapp/proto"
	"userapp/repository"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"go-micro.dev/v4/logger"
)

type UserRegisterRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Name     string `json:"name"`
	Address  string `json:"address"`
}

type UserLoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (_r *UserRegisterRequest) Load(in *pb.UserRegisterRequest) {
	_r.Email = in.Email
	_r.Password = in.Password
	_r.Name = in.Name
	_r.Address = in.Address
}

func (_r *UserRegisterRequest) ToEntity(user *entity.User) {
	user.Email = _r.Email
	user.Password = _r.Password
	user.Name = _r.Name
	user.Address = _r.Address
}

func (request UserRegisterRequest) Validate(ctx context.Context, userRepo repository.UserRepository) error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Email, validation.Required, is.Email),
		validation.Field(&request.Password, validation.Required),
		validation.Field(&request.Name, validation.Required),
		validation.Field(&request.Address, validation.Required),
	)
	if err != nil {
		return err
	}

	err = request.validateIsExist(ctx, userRepo)
	if err != nil {
		return err
	}

	return nil
}

func (_r *UserRegisterRequest) validateIsExist(ctx context.Context, userRepo repository.UserRepository) error {
	user, err := userRepo.GetByEmail(ctx, _r.Email)
	if user != nil {
		logger.Error("email already registered, ", err)
		return errors.New("email already registered")
	}
	return nil
}

func (_r *UserLoginRequest) Load(in *pb.UserLoginRequest) {
	_r.Email = in.Email
	_r.Password = in.Password
}

func (request UserLoginRequest) Validate() error {
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Email, validation.Required, is.Email),
		validation.Field(&request.Password, validation.Required),
	)
	if err != nil {
		return err
	}
	return nil
}
