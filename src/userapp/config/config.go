package config

import (
	"fmt"
	"os"
	"strconv"

	_ "github.com/joho/godotenv/autoload"

	"github.com/pkg/errors"
	"go-micro.dev/v4/config"
	"go-micro.dev/v4/config/source/env"
	"go-micro.dev/v4/logger"
)

type Config struct {
	Port        int
	Tracing     TracingConfig
	DBHost      string
	DBName      string
	DBUser      string
	DBPassword  string
	DBPort      string
	DBSSLMode   string
	SendgridKey string
	UserService string
}

type TracingConfig struct {
	Enable bool
	Jaeger JaegerConfig
}

type JaegerConfig struct {
	URL string
}

var cfg *Config = &Config{
	Port:        GetEnvInt("PORT", 3002),
	DBHost:      GetEnv("DATABASE_CORE_HOST"),
	DBName:      GetEnv("DATABASE_CORE_NAME"),
	DBUser:      GetEnv("DATABASE_CORE_USER"),
	DBPassword:  GetEnv("DATABASE_CORE_PASSWORD"),
	DBPort:      GetEnv("DATABASE_CORE_PORT", "3306"),
	DBSSLMode:   GetEnv("DATABASE_SSL_MODE", "prefer"),
	SendgridKey: GetEnv("SENDGRID_API_KEY"),
	UserService: "userapp",
}

func Get() *Config {
	return cfg
}

func GetEnvInt(key string, defaultval ...int) int {
	val := os.Getenv(key)
	if val != "" {
		valInt, err := strconv.Atoi(val)
		if err != nil {
			logger.Fatal("fail to load or convert env var", val, ":", err)
		}
		return valInt
	}
	if len(defaultval) > 0 {
		return defaultval[0]
	}
	return 0
}

func GetEnv(key string, defaultval ...string) string {
	val := os.Getenv(key)
	if val != "" {
		return val
	}
	if len(defaultval) > 0 {
		return defaultval[0]
	}
	return ""
}

func Address() string {
	return fmt.Sprintf(":%d", cfg.Port)
}

func Tracing() TracingConfig {
	return cfg.Tracing
}

func Load() error {
	configor, err := config.NewConfig(config.WithSource(env.NewSource()))
	if err != nil {
		return errors.Wrap(err, "configor.New")
	}
	if err := configor.Load(); err != nil {
		return errors.Wrap(err, "configor.Load")
	}
	if err := configor.Scan(cfg); err != nil {
		return errors.Wrap(err, "configor.Scan")
	}
	return nil
}
