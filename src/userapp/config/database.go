package config

import (
	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func NewDatabase() *gorm.DB {
	dbHost := Get().DBHost
	dbName := Get().DBName
	dbUser := Get().DBUser
	dbPassword := Get().DBPassword
	dbPort := Get().DBPort
	dbSslMode := Get().DBSSLMode

	dsn := "host=" + dbHost +
		" user=" + dbUser +
		" password=" + dbPassword +
		" dbname=" + dbName +
		" port=" + dbPort +
		" sslmode=" + dbSslMode

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		logrus.SetReportCaller(true)
		logrus.Fatal("Failed connect to database. \n", err)
	}
	logrus.SetReportCaller(false)
	logrus.Info("Connected to database...")
	db.Logger = logger.Default.LogMode(logger.Info)

	return db
}
