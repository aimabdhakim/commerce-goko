package service

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"userapp/entity"
	pb "userapp/proto"
	"userapp/repository"
	"userapp/validation"

	"go-micro.dev/v4/logger"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

var (
	ErrInvalidEmailCredentials    = errors.New("incorrect email")
	ErrInvalidPasswordCredentials = errors.New("incorrect password")
	ErrInactiveUser               = errors.New("inactive user")
	ErrFailToCreatePassword       = errors.New("fail to create password")
	ErrFailToSaveUserData         = errors.New("fail to save user data")
	ErrLoginFailed                = errors.New("login failed")
	ErrUserNotFound               = errors.New("user not found")
	ErrRequiredUserID             = errors.New("user id is required")
)

const (
	UserTypeIDMember int    = 1
	UserTypeIDSales  int    = 2
	ActionLogin      string = "logged in"
)

type userServiceImpl struct {
	repo repository.UserRepository
}

type UserService interface {
	Register(ctx context.Context, in *pb.UserRegisterRequest, resp *pb.UserRegisterResponse) error
	Login(ctx context.Context, in *pb.UserLoginRequest, resp *pb.UserResponse) error
	Profile(ctx context.Context, in *pb.UserDetailRequest, resp *pb.UserResponse) error
}

func (_s *userServiceImpl) Register(ctx context.Context, in *pb.UserRegisterRequest, resp *pb.UserRegisterResponse) error {
	var err error
	var request = &validation.UserRegisterRequest{}
	var user = &entity.User{}
	request.Load(in)
	if err := request.Validate(ctx, _s.repo); err != nil {
		return err
	}

	request.ToEntity(user)
	user.Username = createUsername(user.Email, _s.repo)
	user.Password, err = hashPassword(user.Password)
	if err != nil {
		logger.Error("error hashing password, ", err)
		err = nil
		return ErrFailToCreatePassword
	}

	user.UserTypeID = UserTypeIDMember

	err = _s.repo.Save(ctx, user)
	if err != nil {
		logger.Error("fail save user data, ", err)
		err = nil
		return ErrFailToSaveUserData
	}

	return err
}

func (_s *userServiceImpl) Login(ctx context.Context, in *pb.UserLoginRequest, resp *pb.UserResponse) error {
	var request = &validation.UserLoginRequest{}
	request.Load(in)

	if err := request.Validate(); err != nil {
		return err
	}

	user, err := _s.repo.GetByEmail(ctx, request.Email)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return ErrInvalidEmailCredentials
		}
		logger.Error("login failed, err: ", err)
		return ErrLoginFailed
	}

	if !checkPassword(request.Password, user.Password) {
		logger.Error("user password is invalid", err)
		return ErrInvalidPasswordCredentials
	}

	err = _s.Profile(ctx, &pb.UserDetailRequest{UserId: user.UserID}, resp)
	if err != nil {
		logger.Error(err)
		return ErrUserNotFound
	}

	return err
}

func (_s *userServiceImpl) Profile(ctx context.Context, in *pb.UserDetailRequest, resp *pb.UserResponse) error {
	if in.GetUserId() == "" {
		return ErrRequiredUserID
	}
	user, err := _s.repo.GetByUserID(ctx, in.UserId)
	if err != nil {
		logger.Error("user not found")
		return ErrUserNotFound
	}

	var data = &pb.User{}
	data.UserId = user.UserID
	data.Email = user.Email
	data.Username = user.Username
	data.Name = user.Name
	data.Address = user.Address
	data.Usertypeid = int32(user.UserTypeID)

	resp.Data = data

	return err
}

func createUsername(email string, userRepo repository.UserRepository) string {
	emailSplit := strings.Split(email, "@")
	username := emailSplit[0]

	if len(username) > 32 {
		username = username[:25]
	}

	user, _ := userRepo.GetByUsername(context.Background(), username)
	if user != nil && user.Username == username {
		username = fmt.Sprintf("%s%d", username, rand.Intn(100))
	}

	return username
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 8)
	return string(bytes), err
}

func checkPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func NewUserServiceImpl(repo repository.UserRepository) UserService {
	return &userServiceImpl{
		repo: repo,
	}
}
