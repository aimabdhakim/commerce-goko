package repository

import (
	"context"
	"userapp/entity"

	"gorm.io/gorm"
)

type UserRepository interface {
	GetByUserID(ctx context.Context, userID string) (*entity.User, error)
	GetByUsername(ctx context.Context, username string) (*entity.User, error)
	GetByEmail(ctx context.Context, email string) (*entity.User, error)
	Save(ctx context.Context, userEntity *entity.User) error
	GetDB() *gorm.DB
}

type userRepoImpl struct {
	db *gorm.DB
}

func (_r *userRepoImpl) GetByUserID(ctx context.Context, userID string) (*entity.User, error) {
	userEntity := &entity.User{}
	result := _r.db.WithContext(ctx).Table(userEntity.TableName()).Where("user_id = ?", userID).First(&userEntity)
	if result.Error != nil {
		return nil, result.Error
	}
	return userEntity, nil
}

func (_r *userRepoImpl) GetByUsername(ctx context.Context, userName string) (*entity.User, error) {
	userEntity := &entity.User{}
	err := _r.db.WithContext(ctx).Table(userEntity.TableName()).Where("username = ?", userName).First(&userEntity).Error
	if err != nil {
		return nil, err
	}
	return userEntity, nil
}

func (_r *userRepoImpl) GetByEmail(ctx context.Context, email string) (*entity.User, error) {
	userEntity := &entity.User{}
	err := _r.db.WithContext(ctx).Table(userEntity.TableName()).Where("email = ?", email).First(&userEntity).Error
	if err != nil {
		return nil, err
	}
	return userEntity, nil
}

func (_r *userRepoImpl) Save(ctx context.Context, userEntity *entity.User) error {
	err := _r.db.WithContext(ctx).Table(userEntity.TableName()).Create(&userEntity).Error
	if err != nil {
		return err
	}
	return nil
}

func NewUserRepoImpl(database *gorm.DB) UserRepository {
	return &userRepoImpl{
		db: database,
	}
}

func (_r *userRepoImpl) GetDB() *gorm.DB {
	return _r.db
}
