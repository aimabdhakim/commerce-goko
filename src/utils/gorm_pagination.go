package utils

import (
	"math"
	"regexp"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"go-micro.dev/v4/logger"
	"gorm.io/gorm"
)

type PaginationResponse struct {
	Page      int               `json:"page"`
	PerPage   int               `json:"per_page"`
	PageCount int               `json:"page_count"`
	TotalRows int64             `json:"total_rows"`
	Links     map[string]string `json:"links"`
}

func (_p *PaginationResponse) Default(pagination *Pagination) {
	_p.Page = pagination.Page
	_p.PerPage = pagination.Limit
	_p.PageCount = pagination.TotalPages
	_p.TotalRows = pagination.TotalRows
	var links = make(map[string]string)
	page := strconv.Itoa(pagination.Page)
	limit := strconv.Itoa(pagination.Limit)

	links["self"] = "/?page=" + page + "&limit=" + limit + "&sort=" + pagination.Sort + "&sortcolumn=" + pagination.SortColumn
	links["first"] = "/?page=1&limit=" + limit + "&sort=" + pagination.Sort
	links["last"] = "/?page=" + strconv.Itoa(pagination.TotalPages) + "&limit=" + limit + "&sort=" + pagination.Sort + "&sortcolumn=" + pagination.SortColumn
	if pagination.Page > 1 {
		page := strconv.Itoa(pagination.Page - 1)
		links["prev"] = "/?page=" + page + "&limit=" + limit + "&sort=" + pagination.Sort + "&sortcolumn=" + pagination.SortColumn
	} else {
		links["prev"] = ""
	}
	if pagination.Page < pagination.TotalPages {
		page := strconv.Itoa(pagination.Page + 1)
		links["next"] = "/?page=" + page + "&limit=" + limit + "&sort=" + pagination.Sort + "&sortcolumn=" + pagination.SortColumn
	} else {
		links["next"] = ""
	}
	_p.Links = links
}

type Pagination struct {
	Limit      int    `json:"limit"`
	Page       int    `json:"page"`
	Sort       string `json:"sort"`
	SortColumn string `json:"sortcolumn"`
	Keyword    string `json:"keyword"`
	TotalRows  int64
	TotalPages int
	Result     interface{} `json:"result"`
}

func (_r *Pagination) Load(in any) {
	pagination, err := TypeConverter[Pagination](in)
	if err != nil {
		logger.Error(err)
		return
	}
	var result = Pagination{}
	result = *pagination
	*_r = result
}

func (_r Pagination) Validate() error {
	return validation.ValidateStruct(&_r,
		validation.Field(&_r.Keyword, validation.Match(
			regexp.MustCompile("^[a-zA-Z0-9 -()?,.@!#$%&*+_=;:/]+$"))),
		validation.Field(&_r.Sort, validation.In("asc", "desc")),
		validation.Field(&_r.Limit, validation.Max(100)),
	)
}

// func (_r *Pagination) Response(resp *pb.PaginationResponse)  {
// 	response := &PaginationResponse{}
// 	response.Default(_r)
// 	data, err := TypeConverter[pb.PaginationResponse](response)
// 	if err != nil{
// 		logger.Error(err)
// 		return
// 	}
// 	var result = pb.PaginationResponse{}
// 	result = *data
// 	*resp = result

// }

func NewPagination() *Pagination {
	return &Pagination{}
}

func (_p *Pagination) GetOffset() int {
	return (_p.GetPage() - 1) * _p.GetLimit()
}

func (_p *Pagination) GetPage() int {
	if _p.Page <= 0 {
		_p.Page = 1
	}
	return _p.Page
}

func (_p *Pagination) GetLimit() int {
	if _p.Limit <= 0 {
		_p.Limit = 10
	}
	return _p.Limit

}

func (_p *Pagination) GetSort() string {
	if _p.Sort == "" {
		_p.Sort = "asc"
	}
	return _p.Sort
}

func (_p *Pagination) GetSortColumn() string {
	if _p.SortColumn == "" {
		_p.SortColumn = "created_at"
	}
	return _p.SortColumn
}

func (_p *Pagination) GetKeyword() string {
	return _p.Keyword
}

func Paginate(value interface{}, pagination *Pagination, db *gorm.DB, column string, customTotalRows ...int64) func(db *gorm.DB) *gorm.DB {
	var totalRows int64

	if len(customTotalRows) > 0 {
		totalRows = customTotalRows[0]
	} else {
		db.Model(value).Count(&totalRows)

	}

	if column == "" {
		column = pagination.GetSortColumn()
	}

	pagination.TotalRows = totalRows

	totalPages := int(math.Ceil(float64(pagination.TotalRows) / float64(pagination.GetLimit())))
	pagination.TotalPages = totalPages
	return func(db *gorm.DB) *gorm.DB {
		return db.Offset(pagination.GetOffset()).Limit(pagination.GetLimit()).Order(column + " " + pagination.GetSort())
	}
}
