This is all the table is need for commerce

CREATE TABLE users (
    user_id uuid DEFAULT uuid_generate_v4 (),
    username VARCHAR,
email VARCHAR,
password VARCHAR,
name VARCHAR,
user_type_id SMALLINT NOT NULL,
address VARCHAR,
created_by VARCHAR,
modified_by VARCHAR,
deleted_by VARCHAR,
created_at TIMESTAMP WITH TIME ZONE NOT NULL,
modified_at TIMESTAMP WITH TIME ZONE,
deleted_at TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY (user_id)
);

CREATE TABLE usertype (
    id INT,
    name VARCHAR,
    PRIMARY KEY (id)
);

CREATE TABLE product (
    product_id uuid DEFAULT uuid_generate_v4 (),
    name VARCHAR,
description VARCHAR,
price INT,
owner_id VARCHAR,
owner_name VARCHAR,
created_by VARCHAR,
modified_by VARCHAR,
deleted_by VARCHAR,
created_at TIMESTAMP WITH TIME ZONE NOT NULL,
modified_at TIMESTAMP WITH TIME ZONE,
deleted_at TIMESTAMP WITH TIME ZONE,
    PRIMARY KEY (product_id)
);